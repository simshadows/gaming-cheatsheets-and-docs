# Sim's WvW Builds

*This document will outline the builds that I personally recommend for use. Generally, I will focus on how the builds fit into group settings (havoc groups, zergs, etc.), and will mostly cover group builds.*



## Power Scourge

This build is based on [Metabattle's *Power Scourge* build](https://metabattle.com/wiki/Build:Scourge_-_Power_Scourge).

### Why use this build?

Scourge is WvW's main meta DPS class for medium and zerg-scale group fights, with wide AOEs and extremely punishing burst damage at range.

The main challenge of this build is choosing when and where to lay down your long-cooldown AOEs.

**Group Role:**<br>
Ranged AOE DPS<br>
Downstate Control (if sacrificing some DPS to spec Blood Magic)

**Pros/Cons:**<br>
\+ Damage is delivered in huge spikes.<br>
\+ Damage is delivered in huge-radius AOEs.<br>
\- Important damage abilities are on a roughly 30 second cooldown, so you need to think about when and where to place them.<br>
\- No automatic invulnerabilities. You have a lot of health and barriers, but you may still die to quick focus and Deadeyes.

### Build Defaults Summary

**Slot Skills:**<br>
[Heal] Sand Flare<br>
[Util 1] Trail of Anguish<br>
[Util 2] Well of Suffering<br>
[Util 3] Well of Corruption<br>
[Elite] Ghastly Breach

**Specializations:**<br>
[Spec 1] Spite (1-2-2)<br>
[Spec 2] Soul Reaping (2-1-2)<br>
[Spec 3] Scourge (1-3-1)

**Weapons:**<br>
Axe/Focus *(Superior Sigil of Force + Superior Sigil of Air)*<br>
Staff *(Superior Sigil of Bloodlust + Superior Sigil of Energy)*

**Equipment:**<br>
Mix Berserker, Marauder, and Cavalier stats as desired. (I run Berserker armour/weapons, and Marauder trinkets.);<br>
Full Superior Rune of the Scholar;<br>
Full Mighty WvW Infusion

**Consumables:**<br>
Plate of Truffle Steak<br>
Tin of Fruitcake

### Build Variants Summary

**Out-of-combat life force regeneration:**<br>
[Util 1] Dessicate

**Long-range pull:**<br>
[Util 1] Spectral Grasp

**Extra mobility:**<br>
[Util 3] Sand Swell

**Sacrifice DPS to add downstate control:**<br>
[Spec 1] Blood Magic (1-3-3) *(or (1-1-3) if someone else is already running `Vampiric Presence`)*

### Usage: Outside of Combat

If you find yourself running low on life force, temporarily switch a utility skill to `Dessicate` and spam it as required.

`Sand Swell` is also a useful utility skill to temporarily use for group mobility.

### Usage: During a Siege

A useful thing you can do is laying marks down using Staff 2-5 skills. These are basically traps that can put pressure on enemies, or even alert you if a stealthed enemy walks over it (though it doesn't reveal). Lay these marks down on doorways.

You can also lay marks on walls, but make sure your commander isn't trying to keep a low profile since marks are very visually evident on walls.

You can pull enemies off walls by temporarily switching to the `Spectral Grasp` utility skill.

### Usage: In Combat, During An Engagement

The key to Scourge's insane damage output is the shroud-wells combo to deal a huge damage spike in a large AOE. This is because wells and your shroud are your main sources of AOE spike damage, and the Soul Reaping traits `Soul Barbs` and `Death Perception` grant you 6 seconds of +33% crit chance and +300 ferocity, and 16 seconds of +10% damage, all from the moment you enter shroud.

My recommended opening burst rotation is:

1. Summon a sand shade on top of your enemies (F1 `Manifest Sand Shade`).
2. Enter shroud (F5 `Desert Shroud`).
3. Pop `Well of Suffering`, `Well of Corruption`, and try to land `Ghastly Breach`.
4. (OPTIONAL) Pop the other shade abilities (F2 `Nefarious Favor`, F3 `Sand Cascade`, and F4 `Garish Pillar`). Remember that these can all be used simultaneously with no global cooldown.
5. (OPTIONAL) If you opened with staff, pop Staff 4 and swap to Axe/Focus. Staff 4 is a fairly high-damage AOE skill that also transfers conditions to enemies.

Remember that when you enter shroud, you also pulse damage around both yourself and your sand shade. This is why you summon a sand shade before entering shroud. Additionally, this pulse damage can relocate to new sand shades if you summon new ones while in shroud.

Try to land your sand shade, shroud, wells, and `Ghastly Breach` on top of the highest concentration of enemies you can. Your main spike combo has a cooldown of around 30 seconds (or 40 seconds if you want to wait for `Well of Corruption`). Don't waste it and be patient if you have to. If you're still waiting for the right opportunity to pop it, just fill with Axe/Focus skills.

### Usage: (OPTIONAL) Downstate Control with the Blood Magic Specialization

**Important Note: Please only spec into Blood Magic for downstate control if and only if you know what you are doing. Improper use can accidentally kill your downs by teleporting them into enemy AOEs, or accidentally dodge friendly revives.**

Optionally, you can choose to spec into Blood Magic to gain the ability to gather friendly downs and group revive them. This is achieved using the shade F4 skill `Garish Pillar`.

This can also be comboed with Firebrand's `Merciful Intervention` to revive your downs even faster. Though, this will require some tight coordination.

*(I don't really have an understanding of how best to use this though, so I'll need to come back to this after some more experience with Scourge downstate control.)*



## Dual-Axe Spellbreaker

*This build was originally conceived by [Rubyeclipse](https://twitter.com/RubyEclipse).*

### Why use this build?

**Group Role:**<br>
Melee Cleave DPS

**Pros/Cons:**<br>
\+ Damage is delivered in a mix of high-burst cleave (with Axe 5) and constant low-cooldown cleave.<br>
\+ Lots of active sustain combined with a 4 second automatic invulnerability allows you to be a tanky frontliner.<br>
\- Low mobility. Since you're melee, this presents itself as more of challenge to you compared to if you were a ranged DPS.

### Build Defaults Summary

**Slot Skills:**<br>
[Heal] Healing Signet<br>
[Util 1] Berserker Stance<br>
[Util 2] Balanced Stance<br>
[Util 3] Endure Pain<br>
[Elite] Winds of Disenchantment

**Specializations:**<br>
[Spec 1] Defense (2-1-1)<br>
[Spec 2] Discipline (2-2-1)<br>
[Spec 3] Spellbreaker (1-2-2)

**Weapons:**<br>
Axe/Axe *(Superior Sigil of Force + Superior Sigil of Accuracy)*<br>
Greatsword *(Superior Sigil of Force + Superior Sigil of Accuracy)*

**Equipment:**<br>
*(Mix Berserker, Marauder, and Assassin gear as desired. The below is a suggestion, based on Ruby's build.)*<br>
Armor, Head: Marauder<br>
Armor, Shoulders: Berserker<br>
Armor, Chest: Marauder<br>
Armor, Hands: Berserker<br>
Armor, Legs: Marauder<br>
Armor, Feet: Berserker<br>
Trinkets, Back: Marauder<br>
Trinkets, Accessories: Marauder/Marauder<br>
Trinkets, Amulet: Marauder<br>
Trinkets, Rings: Marauder/Assassin<br>
Weapons, Axe/Axe: Berserker/Marauder<br>
Weapons, Greatsword: Berserker<br>
Full Superior Rune of the Scholar<br>
16x Precise WvW Infusion, 2x Mighty WvW Infusion

**Consumables:**<br>
Plate of Truffle Steak<br>
Tin of Fruitcake

### Build Variants Summary

**More damage:**<br>
[Util 2] Frenzy<br>

### Usage

*(TODO)*



## Vault-Spam Daredevil

### Why use this build?

**Group Role:**<br>
Melee Cleave DPS<br>
Highly-Mobile Scout (killing sentries; sweeping traps; recon; quickly reaching capture circles; activating graves)

**Pros/Cons:**<br>
\+ Damage is delivered constantly, with somewhat no-cooldown attacks.<br>
\+ Lots of evade frames.<br>
\+ High-mobility during fights.<br>
\+ Ridiculously high-mobility outside of fights.<br>
\- Very squishy. If your invulnerabilities are on-cooldown, you will explode to Deadeyes. Or, in between evade frames, you can still get bursted down.

### Build Defaults Summary

**Slot Skills:**<br>
[Heal] Withdraw<br>
[Util 1] Roll for Initiative<br>
[Util 2] Signet of Agility<br>
[Util 3] Shadowstep<br>
[Elite] Basilisk Venom

**Specializations:**<br>
[Spec 1] Critical Strikes (3-2-3)<br>
[Spec 2] Acrobatics (1-3-2)<br>
[Spec 3] Daredevil (1-1-3)

**Weapons:**<br>
Staff *(Superior Sigil of Force + Superior Sigil of Accuracy)*<br>
Short Bow *(Superior Sigil of Absorption + Superior Sigil of Energy)*

**Equipment:**<br>
Mix Berserker and Marauder gear as desired. (I run practically all Marauder.)<br>
Full Superior Rune of the Scholar<br>
Full Mighty WvW Infusion

**Consumables:**<br>
Plate of Truffle Steak<br>
Tin of Fruitcake

### Build Variants Summary

**Super-mobility (for scouting):**<br>
[Util 2] Infiltrator's Signet<br>
[Spec 3] Daredevil (x-x-2)

**Extra defense (best for larger fights):**<br>
[Util 1] Bandit's Defense<br>
[Elite] Dagger Storm<br>
[Spec 1] Critical Strikes (x-x-3)

**Better for hit-and-runs and duels:**<br>
[Spec 1] Trickery (3-1-2)

### Usage

*(TODO)*



## Power Herald

This build is based on [Metabattle's *Hammer Backline* build](https://metabattle.com/wiki/Build:Herald_-_Hammer_Backline).

### Why use this build?

**Group Role:**<br>
Ranged AOE DPS<br>
10-target Perma-Swiftness<br>
Minor Boon Support (10-target perma-fury and 8x perma-might)

**Pros/Cons:**<br>
\+ Damage is delivered constantly, with low-cooldown attacks.<br>
\- Damage is mostly delivered in a line between you and your target, which isn't as wide as Power Scourge.<br>
\- Very squishy.<br>
\- No automatic invulnerabilities. Combined with the squishiness, you will die to Deadeye and burst damage quickly.

### Build Defaults Summary

**Slot Skills:**<br>
[Legend 1] Legendary Dragon Stance<br>
[Legend 2] Legendary Assassin Stance

**Specializations:**<br>
[Spec 2] Invocation (2-3-3)<br>
[Spec 1] Devastation (2-3-1)<br>
[Spec 3] Herald (3-1-3)

**Weapons:**<br>
Hammer *(Superior Sigil of Force + Superior Sigil of Accuracy)*<br>
Staff *(Superior Sigil of Bloodlust + Superior Sigil of Energy)*

**Equipment:**<br>
Mix Berserker, Marauder, and Cavalier stats as desired. (I run Berserker armour and weapons, and Marauder trinkets.);<br>
Full Superior Rune of the Scholar;<br>
Full Mighty WvW Infusion

**Consumables:**<br>
Plate of Truffle Steak<br>
Tin of Fruitcake

### Build Variants Summary

**Siege Alacrity (NOTE: This takes a long time to set up.):**<br>
[Legend 1] Legendary Centaur Stance<br>
[Spec 1] Salvation (x-x-x)

### Usage

*(TODO)*



## Healer Herald

This build is based on [Metabattle's *Ventari Healer* build](https://metabattle.com/wiki/Build:Herald_-_Ventari_Healer_-_(WvW)).

### Why use this build?

The Exotic Clerics gear is cheap, and the build is stupidly simple to play. Excellent for players looking for an easy support build to jump into.

Effective use of this build involves being able to manage the positioning of both yourself and the tablet, and energy management. This can get challenging particularly due to only being able to move the tablet every 3 seconds, the tablet's movement speed, and the short range of the healing abilities. Being able to manage these challenges requires patience, and to pay close attention to how your group, individual group members, and the enemy group move to inform your decisions on tablet movement and energy expenditure.

**Group Roles:**<br>
Healer<br>
5-target Perma-Swiftness (outside of combat)<br>
Siege Alacrity (62% Alacrity uptime)

**Additional Group Roles:**<br>
Minor Boon Support (PvE Alacrity, and *pre-stacked* Regeneration, Fury, Might, Swiftness, and Protection)<br>
Siege Golem Support (Swiftness, Superspeed, Alacrity, and projectile-destroy domes)

**Pros/Cons:**<br>
\+ The build is focused on constantly-available high-output healing.<br>
\+ Tablet moving/healing abilities can be activated even when you're crowd-controlled.<br>
\+ You can heal from fair range away by moving the tablet. (But do it with care!)<br>
\+ Tanky due to toughness and high damage mitigation.<br>
\- The healing is awkward and can take some getting used to to control. The tablet may not keep up if the group moves fast, and the heals have short range.<br>
\- It's a support with no stability. You'll still need Firebrands, and you better hope you don't tail behind and get picked off.<br>
\- No automatic invulnerabilities. You are tanky, but you may still die to quick focus and Deadeyes.

### Build Defaults Summary

**Slot Skills:**<br>
[Legend 1] Legendary Dragon Stance<br>
[Legend 2] Legendary Centaur Stance

**Specializations:**<br>
[Spec 1] Retribution (2-1-2)<br>
[Spec 2] Salvation (3-3-1)<br>
[Spec 3] Herald (1-1-1)

**Weapons:**<br>
Hammer *(Superior Sigil of Life + Superior Sigil of Transference)*<br>
Mace/Shield *(Superior Sigil of Transference + Superior Sigil of Renewal)*

**Equipment:**<br>
Full Cleric stats;<br>
Full Superior Rune of the Monk;<br>
Full Healing WvW Infusion

**Consumables:**<br>
Delicious Rice Ball<br>
Bountiful Maintenance Oil

**World Ability Points:**<br>
You shouldn't expect to use siege weapons since you provide siege Alacrity. Instead of ram/cata/etc. upgrades, take maximum supply capacity and build/repair speed.

### Build Variants Summary

**Gain energy per Stance swap, at the cost of strong personal survivability:**<br>
[Spec 1] Invocation (1-1-2)

**Powerful opening damage mitigation for 5 players:**<br>
[Legend 1] Legendary Dwarf Stance<br>
[Spec 1] Invocation (1-1-2)

**More personal survivability (better under high-pressure fights, or small-scale roaming with 5 or less players):**<br>
Add more Minstrel stats

### Usage: Abilities/Weapons Crash-Course

The `Legendary Centaur Stance` is your "healing stance". Centaur Stance abilities will be discussed in the `Combat Healing` section of this guide.

The `Legendary Dragon Stance` is what you use outside of combat, and situationally during combat. Dragon Stance abilities are all upkeep abilities which can also be "consumed" for free to cause additional effects (putting the ability on cooldown). During upkeep, each Dragon Stance ability pulses a particular boon every 3 seconds to 5 people.

For convenience, I will call each Dragon Stance ability by the boon it provides:

- [Heal Slot] "Regen Facet" (`Facet of Light/Infuse Light`) provides the Regeneration boon during upkeep. Importantly, consuming will make you effectively invulnerable for 3 seconds.
- [Utility Slot] "Fury Facet" (`Facet of Darkness/Gaze of Darkness`) provides the Fury boon during upkeep. Importantly, consuming is an AOE reveal.
- [Utility Slot] "Might Facet" (`Facet of Strength/Burst of Strength`) provides the Might boon during upkeep. Consuming gives you 15% extra damage for 5 seconds, which is nice to have for PvE, but pointless during engagements with enemies.
- [Utility Slot] "Swiftness Facet" (`Facet of Elements/Elemental Blast`) provides the Swiftness boon during upkeep. This is useful for PvE breakbar damage.
- [Elite Slot] "Protection Facet" (`Facet of Chaos/Chaotic Release`) provides the Protection boon during upkeep. Consuming is does a powerful knockback and grants Superspeed, both of which are incredibly useful. Also useful for PvE breakbar damage.

Your important abilities with mace and shield are Shield 4 and Shield 5 (which I will discuss further in the `Combat Healing` section of this guide). The mace isn't too useful and you can almost substitute it for a sword. However, Mace 2 provides a fire combo field, and Mace 3's blast finisher comboed with the Centaur Stance's projectile-destroy dome provides an AOE condi cleanse if you need it, or the Mace 2 combo field for AOE Might application. (Though, Centaur Stance's `Purifying Essence` is a much easier and more efficient condition cleanse, so prioritize that.)

Your hammer is mostly there for tagging enemies for rewards since it's ranged, but don't expect to do a tonne of damage with it. And with `Superior Sigil of Life`, you gain stacks of additional Healing Power which applies even when you switch to mace and shield. Hammer 5 is a powerful PvE breakbar ability.

Weapon-swapping from hammer to mace and shield also provides a small amount of healing, so consider weapon-swapping to maximize healing.

Further discussion of the usage of your abilities will be discussed below.

### Usage: Outside of Combat

Outside of combat, you should focus on stacking boons by maintaining Facets from the Legendary Dragon Stance.

You must keep some combination of facets active at all times, maximizing your energy usage. This can depend on what boons your group already provides, but you should prioritize Regeneration, Fury, and Swiftness, and don't bother with Might and Protection.

If one of those boons is already served by other members of your group, switch the Facet out for the Might Facet.

Note that the Protection Facet is really expensive and on its own, its upkeep will use up all your energy. Use this Facet if Regeneration, Fury, and Swiftness are already all provided by your group.

### Usage: Just before combat, and shortly after engaging

Just before your group engages an enemy group, you can activate additional Facets and consume some or all of them before switching to Centaur stance.

The most common case is where you're upkeeping some combination of Regeneration, Fury, Might, and Swiftness outside of combat. In this case, right before your group engages the enemy group, activate the Protection Facet. This will drain your energy at a rate of 4% per second. Starting from 50% energy, you have 12 seconds before your energy reaches 0%. Remember that your Facets deactivate if you allow your energy to drain completely, so plan well, and if you need to, consume some facets early.

Before your energy drains completely, you have a choice of what Facets to consume and when. My general recommendation is to just consume the Regeneration, Fury, and Protection Facets. The Might and Swiftness Facets have a channel time for consumption, which can critically delay your transition into the Legendary Centaur Stance.

When consuming the Protection Facet, you will cast its effects roughly in a short-ranged cone *(or something like that? I honestly have no clue...)* in front of you. Try to aim it at either friendly players or enemy players. It provides the Superspeed buff to friendlies (giving them the option to move more aggressively), and does a very powerful knockback on enemies, which can really turn the tide right at the moment your group engages.

Despite the channel time, there may still be opportunities to consume the Might and Swiftness facets if you sense that your group doesn't urgently need your Centaur Stance healing. However, this is up to your judgement.

### Usage: Combat Healing

#### Basic Moveset

To heal, you must be in the Legendary Centaur Stance. Also, spawn the tablet as required.

You have 5 primary options for healing:

- Moving your tablet will heal friendly players it moves over, but you can only move your tablet every 3 seconds. This is your cheapest and most energy-efficient heal ability.
- Shield 4 is almost as efficient as tablet-moving, but has a 15 second cooldown. Also provides 2.5 seconds of Protection.
- `Natural Harmony` is your source of burst-healing. It is about 65% as energy-efficient as tablet-moving, but provides an incredibly large amount of healing in a short amount of time, especially when spammed. This ability has a 1 second delay before the heal activates.
- Swapping from hammer to mace/shield provides about 1000 healing. This obviously has no energy cost.
- The tablet passively pulses a small amount of healing around it every few seconds.

Prioritize your efficient heals (tablet-moving, Shield 4, and weapon-swapping) before using `Natural Harmony`, and try to save energy for when you need it the most. Also remember that many of these abilities can be used simultaneously (e.g. tablet-moving while popping Shield 4 and `Natural Harmony`), which can provide even more high-output healing when needed.

In addition to your primary healing options, you have 3 primary utility optons:

- `Purifying Essence` is your main condition cleanse. Useful, but very expensive for what it does, so I suggest prioritizing healing, and only cleanse conditions if you have lots of energy, or otherwise at your own discretion.
- `Protective Solace` is a small projectile-destroy dome with an upkeep cost. It is also a Light combo field, making it an excellent way to quickly condi-cleanse your group if you allow your team to shoot projectiles through it or use blast finishers on it!
- `Energy Expulsion` knocks back enemies and destroys the tablet. Also provides "healing fragments", though I don't consider them particularly useful. You only want to use this for its knockback during emergencies.

And finally, you have two *"oh, shit!"* options:

- Shield 5 will block all incoming attacks for 3 seconds, but leaves you immobile. However, conditions you already have on you will continue to damage you, so be very mindful of when you use it.
- Switching back to Legendary Dragon Stance and consuming the Regeneration Facet. This converts all incoming damage into healing for 3 seconds, making you effectively invulnerable. However, this critically puts you out of your Centaur Stance, meaning you don't have effective heals for 10 seconds. *During these 10 seconds, you are effectively useless to your group until you can switch back to Centaur Stance.* Think very carefully about when you want to switch to Dragon Stance!

#### Optional Moveset

A particular skill combination you may also want to consider is `Protective Solace`'s light field combined with your Mace 3 skill's blast finisher. This can generally remove just one condition from friendly players, but if aimed correctly, you might even be able to cleanse two conditions.

Assuming you pulse the dome quickly, you can expect to spend about 20 energy in total. This makes it an inefficient condition cleanse that also takes a lot of effort to pull off, but it's an option if you really need it that also does a bit of damage to enemies.

#### Further Discussion

Assuming you understand your moveset, it should be easy to figure out the basic usage patterns of this build. At this point, you should just try the build and learn from there.

However, here are a few quick pointers:

- `Natural Harmony` has a 1 second delay before it pulses its heal, but it can be activated even while moving the tablet. You can use this to time `Natural Harmony`'s heal to pulse on arrival.
- Open a fight with hammer so you can be ready to swap weapons to pop `Superior Sigil of Renewal`'s heal-on-weapon-swap.
- If you don't expect to use the Shield 4 heal for a while, consider swapping back to hammer so you can use the weapon-swapping heal again when needed.
- This is much easier said than done, but try to save energy for when you need to burst `Natural Harmony`.
- Seriously don't underestimate the usefulness of destroying your tablet with `Energy Expulsion` to prevent stomps, or if a downed friendly is surrounded and being cleaved. It's highly situational, but when it's appropriate, it's a lifesaver.

### Usage: PvE

Unless you're the only healer, you should usually focus on maintaining Facets from the Legendary Dragon Stance since you usually shouldn't need to heal when you're not in combat with players. Healers of other classes should *usually* be enough. However, if someone's dying, switch to Legendary Centaur Stance and heal them.

Key Breakbar damage:

- Dragon Stance "Protection Facet" Consumption *(`Chaotic Release`; 232 break)*
- Dragon Stance "Swiftness Facet" Consumption *(`Elemental Blast`; 179 break)*
- Dragon Stance "Fury Facet" Consumption *(`Gaze of Darkness`; 100 break)*
- Centaur Stance Elite Skill *(`Energy Expulsion`; 150 break)*
- Hammer 5 *(`Drop the Hammer`; 200 break)*

My breakbar rotation is:

1. Start in Legendary Dragon Stance. Use Hammer 5.
2. Consume the Protection, Swiftness, and Fury Facets.
3. Switch to Legendary Centaur Stance.
4. Put the tablet over your allies and use `Natural Harmony` once to give everyone a bit of Alacrity.
5. Spam `Natural Harmony` again until you're out of energy (to stack Alacrity on everyone).
6. Shatter your tablet with `Energy Expulsion`.
7. Respawn your tablet and shatter it over and over again until the breakbar is broken.

### Usage: Siege Alacrity

At a basic level, this is a simple task: place your tablet near the players manning the siege weapons, and spam `Natural Harmony` to grant Alacrity. Ensure that all of them are receiving Alacrity, and reposition the tablet as required.

However, you have two key limitations:

- At equilibrium (i.e. blindly spamming `Natural Harmony` as often as possible to consume your energy as quickly as it regenerates), you can only provide about 63% Alacrity uptime.
- `Natural Harmony` can only pulse Alacrity to 5 players at a time.

While it's probably good enough to naively spam `Natural Harmony` and simply ask players who aren't using siege weapons to stand away from the tablet to ensure Alacrity is always granted to the players operating the siege, there is still room for optimization.

**0: Simply bring more Healer Heralds.**

With two Healer Heralds, you can provide 100% Alacrity uptime if both Heralds simply spam `Natural Harmony`.

However, in smaller groups, this may not be a luxury. The rest of this discussion will talk about how to optimize Alacrity output if you're the only player applying Alacrity.

**1: If the siege weapons are placed far apart, ask the siege operators to stand closer to your tablet.**

Siege weapons are often placed apart to reduce the impact of Siege Disablers and other enemy AOE effects. However, this works against the short range of `Natural Harmony`. If you can't apply Alacrity to all siege operators, you will just have to pick the best place to put your tablet.

But if siege operators are able to stand closer to the tablet while operating the siege weapons, that can make the difference.

**2: Save energy for when players move away from the siege weapons.**

You start with 50% energy outside of combat, allowing you to theoretically provide a burst of about 16 seconds of effectively 100% Alacrity uptime if you simply drain it as fast as possible.

So if people are still building siege weapons or otherwise still waiting to move away, don't waste your energy. Pulse Alacrity only when you're above 45% energy until everyone's moved off, then spam it.

Keep in mind, however, that you may not have the time to save energy, such as when hitting paper keeps/towers. For example, sometimes, by the time everyone's cleared away from the rams, the gate may already be at 40% health and will be breached before your energy runs out from Alacrity spam. If you can see this happening, start draining your energy earlier.

**3: Get in combat with gates/cannons as soon as possible.**

As your group moves towards a gate to ram it, immediately hit the gate to get in combat. Or if your group is moving towards a wall to breach it with catapults and there are cannons/guards/etc. in range, immediately hit them. Hammer 5 is a useful skill for this since it has a long range, and you can just walk away while casting it.

This will allow you to continue filling up your energy bar to 100%, giving you potentially 33 seconds of 100% Alacrity.

However, keep in mind that you will lose combat with the gate/cannon very quickly if you don't keep hitting the cannon. This is problematic when constructing siege. My recommendation is to hit the gate/cannon, and spend a bit of supply before hitting the gate/cannon again. This can be made easier by investing World Abilty Points into Build Master in order to build siege faster.

### Usage: Golem Rush Support

If your group is running siege golems, your job is to maintain swiftness as necessary, and to use the Superspeed from consuming the Protection Facet in order to allow stragglers to catch up with the group. (Or, if the group is already together, grant them all Superspeed!)

Occasionally, when the golems are under attack, it might also be useful to switch to Centaur Stance and use the projectile-destroy dome or pulse condition cleanses to prevent them from taking damage.

### Appendix: Theorycrafting Stance-Alternation

I'm currently considering how effective a playstyle of frequent (or even on-cooldown) stance-swapping can be. In theory, this allows a bit of in-combat boon support and higher overall healing output if played a particular way (due to stance-swapping granting 25% energy per swap, and Herald x-x-1 healing players you give boons to). However, your burst healing will be a lot less readily-available, which can frequently be the difference between victory and defeat during a fight.

Herald x-x-2 may also make this playstyle work better instead of Herald x-x-1.

### Appendix: Siege Alacrity Uptime

Siege Alacrity uptime is calculated from the following parameters: 2.5 second boon duration, 20% energy per usage of Natural Harmony, 5%/second energy regeneration, and assuming we start at 0% energy. This allows Natural Harmony to be used every 4 seconds, making the Alacrity uptime 62.5%.

However, since you usually start with 50% energy, if we convert the discrete Natural Harmony usage into a continuous rate such that it provides continuous 100% Alacrity uptime, Natural Harmony consumes energy at a rate of (20%)/(2.5 seconds), or 8%/second. The net energy loss is therefore 3%/second, which will consume 50% energy in 16.6 seconds *in theory*.

### Appendix: Other Notes

I don't know exactly how the healing fragments from Energy Expulsion works. If each fragment only heals one player, then I don't think it's very useful in the middle of a fight. If each fragment heals in an AOE, then it becomes significantly more useful! I'm gonna have to test this out some time...

Also, I'm still figuring out how to time Alacrity to catapult/trebuchet volleys and ram swings to further optimize siege Alacrity.



## Support Scourge (LEGACY BUILD - NOT RECOMMENDED)

This build is based on [Metabattle's *Blood Magic Support* build](https://metabattle.com/wiki/Build:Scourge_-_Blood_Magic_Support).

*Honestly, this build might not really be worth running. I feel like its healing/barriers are much weaker than other builds, and its focus on downstate control is not too useful in the havoc groups (10-20 players) I run with. It does do well though if you can heal/barrier/revive while simultaneously successfully applying wide-area condi-pressure, making up for its low healing by applying a fairly high amount of damage in total for a support. However, this will kinda make this build more of a jack-of-all-trades that isn't quite as useful as a Celestial Scourge, or more specialized classes.*

*I might come back to reevaluating this build later after I try other support builds.*

### Why use this build?

**Group Role:**<br>
Downstate Control<br>
Weak Barrier/Healer Support<br>
Weak Large-AOE Condi DPS

**Pros/Cons:**<br>
*(TODO)*

### Build Summary

**Slot Skills:**<br>
[Heal] Sand Flare<br>
[Util 1] Trail of Anguish<br>
[Util 2] Dessicate<br>
[Util 3] Sand Swell<br>
[Elite] Ghastly Breach<br>

**Specializations:**<br>
[Spec 1] Blood Magic (1-1-3)<br>
[Spec 2] Soul Reaping (2-2-3)<br>
[Spec 3] Scourge (1-1-1)

**Weapons:**<br>
Staff *(Superior Sigil of Life + Superior Sigil of Transference)*<br>
Scepter/Torch *(Superior Sigil of Transference + Superior Sigil of Energy)*

**Equipment:**<br>
Full Apothecary stats;<br>
Full Superior Rune of the Monk;<br>
Full Healing WvW Infusion

**Consumables:**<br>
Delicious Rice Ball<br>
Bountiful Maintenance Oil

### Usage

*(TODO)*



