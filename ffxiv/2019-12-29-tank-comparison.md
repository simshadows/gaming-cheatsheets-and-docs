# TLDR

Do note that I come from the viewpoint of a casual player who just wants to taste everything the game has to offer. I probably won't learn fights, so situational abilities will be favoured less. (I'm looking at you, Dark Knight...)

I also don't focus on DPS as a tank, though DPS may be considered under rare circumstances.

## Level 0-29 (Casual Play)

**Warrior**
- **Slightly better mob control.** Your core AOE (*Overpower*) is a frontal cone instead of a circle like the other tanks. This cone has a greater reach than the circle, making it better for collecting aggro, and giving the Warrior more options than the other tanks.

**Paladin**
- **Has a chain stun.** From Level 10, you get *Shield Bash*. This is a stun/interrupt without a cooldown.

**Dark Knight**
- **Most useless.** No notable utilities over the other tanks.

**Gunbreaker**
- **Superior main tank.** From Level 6, you get *Camouflage*. This is a 10% Mitigation (20s every 90s).

## Level 30-49 (Casual Play)

**Warrior**
- **Superior main tank.** *Thrill of Battle* heals 20% HP and increases incoming healing by 20% (10s every 90s).
- **Slightly better mob control.** *(SEE PREVIOUS.)*

**Paladin**
- **Decent main tank.** Paladin has access to blocking, and *Sheltron* blocks everything for 6 seconds. Though, blocking is inconsistent (even with *Sheltron*) since it only works when you're facing the enemy, and many attacks are unblockable. Also, you only mitigate roughly 25% damage when you block an attack.
- **Has a chain stun.** *(SEE PREVIOUS.)*

**Dark Knight**
- **Weak main tank.** No notable utilities over the other tanks.

**Gunbreaker**
- **Great main tank.** *Camouflage* mitigates damage by 10% (20s every 90s).

## Level 50-69 (Casual Play)

**Warrior**
- **Vastly superior main tank.** Tonnes of powerful personal mitigation/sustain (*Thrill of Battle*, *Raw Intuition*, and *Equilibrium*), and personal invulnerability is on a much shorter cooldown than other tanks.
- **No group support.** (Technically you do, but it's at Level 68. Very late.)
- **Slightly better mob control.** *(SEE PREVIOUS.)*

**Paladin**
- **Decent main tank.** On top of blocking, some of your group support abilities can be used for personal mitigation.
- **Vastly superior group support.** You have a Raid-Wide 10% Barrier (*Divine Veil*), an expensive heal spell to use during emergencies when the healer fails to keep up, and a damage interception skill (*Cover*) to save someone if they're being stoopid. A single-target damage mitigation (*Intervention*) also becomes available a bit later (at Level 62). Consistent interrupts also contributes since the Paladin can sometimes cancel enemy AOEs from hitting the group.
- **Has a chain stun.** *(SEE PREVIOUS.)*

**Dark Knight**
- **Weak main tank.** Offers nothing except a personal 20% Magic Mitigation (*Dark Mind*) which can help with bosses, but difficult to use against trash. Personal magic mitigation is difficult to make the most of in casual play.
- **No group support.**

**Gunbreaker**
- **Good main tank.** Has a bit of mitigation (*Camouflage*) and a weak single-target heal-over-time. Group support abilities can also be used for personal mitigation.
- **Some support.** From Level 64, you get a Raid-Wide 10% Magic Mitigation (*Heart of Light*). A bit late in the Level 50-70 skew (hence I labelled it "Some support" instead of "Good support"), but it's there.

## Level 70-79 (Casual Play)

(TODO)

## Level 80 (End-Game, Hardcore Play)

*I don't know how people play at end-game lol*

# Ability Summary

Do note, these are very strong simplifications of the actual abilities. For example, invulnerabilities don't make you invulnerable to certain attacks.

Also, I don't know how blocking and Sheltron works, but I have been told that a successful block will typically reduce damage by 25%. I don't know if Sheltron blocks on a single attack or multiple attacks, and whether or not blocking also affects magic attacks.

## General Self-Sustain

This section includes mitigation abilities intended for support/off-tank usage since they are also relevant.

**Warrior**
- `[lvl 30]` 20% HP Heal + +20% incoming healing (10s every 90s) (*Thrill of Battle*)
- `[lvl 56]` 20% Mitigation OR Lifesteal (6s every 25s) (*Raw Intuition*/*Nascent Flash*) **[SUPPORT OVERLAP]**
- `[lvl 58]` 1200-Potency Heal (every 60s) (*Equilibrium*)
- `[lvl 68]` 12% HP Barrier (15s every 90s) (*Shake It Off*) **[SUPPORT OVERLAP]**

**Paladin**
- `[lvl 35]` (???) 100% Block (6s, costs 50 resource) (*Sheltron*)
- `[lvl 56]` 10% HP Barrier (30s every 90s) (*Divine Veil*) **[SUPPORT OVERLAP]**
- `[lvl 58]` 1200-Potency Heal (2000 mana) (*Clemency*) **[SUPPORT OVERLAP]**

**Dark Knight**
- `[lvl 45]` 20% Magic Mitigation (10s every 60s) (*Dark Mind*)
- `[lvl 70]` 25% HP Barrier (7s every 15s, costs 3000 mana) (*The Blackest Night*) **[SUPPORT OVERLAP]**
- `[lvl 76]` 10% Magic Mitigation (15s every 90s) (*Dark Missionary*) **[SUPPORT OVERLAP]**

**Gunbreaker**
- `[lvl 6]` 10% Mitigation (20s every 90s) (*Camouflage*)
- `[lvl 45]` 1200-Total-Potency HOT (18s every 60s) (*Aurora*) **[SUPPORT OVERLAP]**
- `[lvl 64]` 10% Magic Mitigation (15s every 90s) (*Heart of Light*) **[SUPPORT OVERLAP]**
- `[lvl 68]` 15% Mitigation (7s every 25s) (*Heart of Stone*) **[SUPPORT OVERLAP]**

## General Support

**Warrior**
- **(NONE)**

**Paladin**
- `[lvl 45]` Single-Target 100% Damage Transfer To Self (12s every 120s) (*Cover*)
- `[lvl 58]` Single-Target 1200-Potency Heal (2000 mana) (*Clemency*) **[SELF-SUSTAIN OVERLAP]**

**Dark Knight**
- **(NONE)**

**Gunbreaker**
- `[lvl 45]` Single-Target 1200-Total-Potency HOT (18s every 60s) (*Aurora*) **[SELF-SUSTAIN OVERLAP]**


## Personal Invulnerability

For Warrior/Dark Knight/Gunbreaker, the invulnerability is not a true invulnerability. For these jobs, their "invulnerability" abilities only prevent health from dropping below 1 HP.

For Paladin, the invulnerability is a true invulnerability that mitigates damage by 100%.

- `[lvl 42]` **Warrior** (8s every 240s) (*Holmgang*)
- `[lvl 50]` **Paladin** (10s every 420s) (*Hallowed Ground*)
- `[lvl 50]` **Dark Knight** (10s every 300s) (*Living Dead*)
- `[lvl 50]` **Gunbreaker** (8s every 360s) (*Superbolide*)


## Single-Target Mitigation

**Warrior**
- `[lvl 76]` 10% Mitigation + Heal (6s every 25s) (*Nascent Flash*) **[SELF-SUSTAIN OVERLAP]**

**Paladin**
- `[lvl 62]` 10% Mitigation + 10% Mitigation if target has *Rampart* + 15% Mitigation if target has *Sentinel* (6s, costs 50 resource) (*Intervention*)

**Dark Knight**
- `[lvl 70]` 25% HP Barrier (7s every 15s, costs 3000 mana) (*The Blackest Night*) **[SELF-SUSTAIN OVERLAP]**

**Gunbreaker**
- `[lvl 68]` 15% Mitigation (7s every 25s) (*Heart of Stone*) **[SELF-SUSTAIN OVERLAP]**


## Raid AOE Damage Mitigation

**Warrior**
- `[lvl 68]` Raid-Wide 12% HP Barrier (15s every 90s) (*Shake It Off*) **[SELF-SUSTAIN OVERLAP]**

**Paladin**
- `[lvl 56]` Raid-Wide 10% of the caster's HP as a Barrier (30s every 90s) (*Divine Veil*) **[SELF-SUSTAIN OVERLAP]**
- `[lvl 70]` You stop attacking/moving + (???) 100% Block + 15% Mitigation to friendlies behind you (18s every 120s) (*Passage of Arms*)

**Dark Knight**
- `[lvl 76]` 10% Magic Mitigation (15s every 90s) (*Dark Missionary*) **[SELF-SUSTAIN OVERLAP]**

**Gunbreaker**
- `[lvl 64]` 10% Magic Mitigation (15s every 90s) (*Heart of Light*) **[SELF-SUSTAIN OVERLAP]**


# Warrior

## Unique Self-Sustain


**Thrill of Battle (Level 30) (90s Recast)**

```
Increases maximum HP by 20% and restores the amount increased.
Additional Effect: Increases HP recovery via healing actions on self by 20%
Duration: 10s
```

Not really a mitigation, but helps the healers out a tonne during emergencies. Usually popped at mid-health.


**Vengeance (Level 38) (120s Recast)**

```
Reduces damage taken by 30% and delivers an attack with a potency of 55 every time you suffer physical damage.
Duration: 15s
```

Standard mitigation. Particularly useful in large pulls (due to retaliation damage).


**Holmgang (Level 42) (240s Recast)**

```
Brace yourself for an enemy onslaught, preventing most attacks from reducing your HP to less than 1.
Duration: 8s
When a target is selected, halts their movement with chains.
```

Emergency invulnerability at low health.

Very difficult to use well, but amazing during emergencies.

And yes, I've used this successfully a few times :)


**Raw Intuition (Level 56) (25s Recast)**

```
Reduces damage taken by 20%.
Duration: 6s
Shares a recast timer with Nascent Flash.
```

Standard mitigation. However, it is mutually exclusive with Nascent Flash at Level 76.


**Equilibrium (Level 58) (60s Recast)**

```
Restores own HP.
Cure Potency: 1,200
```

Standard self-heal.


**Shake It Off (Level 68) (90s Recast)**

```
Creates a barrier around self and all nearby party members that absorbs damage totaling 12% of maximum HP.
Dispels Thrill of Battle, Vengeance, and Raw Intuition, increasing damage absorbed by 2% for each effect removed.
Duration: 15s
```

Standard raid-wide barrier that also works on yourself!


**Nascent Flash (Level 76) (25s Recast)**

```
Grants Nascent Flash to self and Nascent Glint to target party member.
Nascent Flash Effect: Absorbs damage dealt as HP
Nascent Glint Effect: Restores HP equaling 50% of that recovered by Nascent Flash while also reducing damage taken by 10%
Duration: 6s
Shares a recast timer with Raw Intuition.
```

Mix of personal mitigation and support. (I will discuss support elsewhere.)

This ability shares a recast with Raw Intuition, but I think Nascent Flash might be generally superior due to the lifesteal and increased personal single-target DPS (at Level 80 due to interactions with the ability Inner Chaos). However, I imagine Raw Intuition will still have its uses, especially when you can't make full use of the self-heal (due to your burst DPS cooldowns not being available).


## Unique Support


**Shake It Off (Level 68) (90s Recast)**

```
Creates a barrier around self and all nearby party members that absorbs damage totaling 12% of maximum HP.
Dispels Thrill of Battle, Vengeance, and Raw Intuition, increasing damage absorbed by 2% for each effect removed.
Duration: 15s
```

Standard raid-wide barrier.


**Nascent Flash (Level 76) (25s Recast)**

```
Grants Nascent Flash to self and Nascent Glint to target party member.
Nascent Flash Effect: Absorbs damage dealt as HP
Nascent Glint Effect: Restores HP equaling 50% of that recovered by Nascent Flash while also reducing damage taken by 10%
Duration: 6s
Shares a recast timer with Raw Intuition.
```

Mix of personal mitigation and support. (I discussed personal mitigation elsewhere.)

This is useful in an off-tank role, where you can provide very minor heals to the main tank.




# Paladin

## Unique Self-Sustain


**Sheltron (Level 35)**

```
Block incoming attacks.
Duration: 6s
Oath Gauge Cost: 50
```

I don't know how blocking works... is it only physical damage? Does it mitigate damage by 100%? Can you block while attacking/casting?

Regardless though, this ability seems to be a high-investment high-reward ability. The cost is really huge for a short-duration block. But, assuming it fully mitigates, this can be extremely strong when used correctly. Your healers will thank you.


**Sentinel (Level 38) (120s Recast)**

```
Reduces damage taken by 30%.
Duration: 15s
```

Standard mitigation.


**Hallowed Ground (Level 50) (420s Recast)**

```
Renders you impervious to most attacks.
Duration: 10s
```

Is this 100% damage mitigation? Holy hell that seems crazy strong.


**Divine Veil (Level 56) (90s Recast)**

```
Upon HP recovery via healing magic cast by self or a party member, a protective barrier is cast on all party members within a radius of 15 yalms.
Duration: 30s
Barrier Effect: Prevents damage up to 10% of your maximum HP
Duration: 30s
Effect ends upon additional HP recovery via healing magic.
```

It's a raid-wide barrier, so it affects yourself too!


**Clemency (Level 58) (2000 Mana)**

```
Restores target's HP.
Cure Potency: 1,200
Additional Effect: Restores to self 50% of HP restored to target if target is a party member
```

Expensive single-target heal that you can use on yourself in an emergency.


**Passage of Arms (Level 70) (120s Recast)**

```
Increases block rate to 100% and creates a designated area in a cone behind you in which party members will only suffer 85% of all damage inflicted.
Duration: 18s
Effect ends upon using another action or moving (including facing a different direction).
Cancels auto-attack upon execution.
```

It's more of a support ability since you can't do anything while blocking, but it's still a defensive option.


## Unique Support


**Shield Bash (Level 10)**

```
Delivers an attack with a potency of 110.
Additional Effect: Stun
Duration: 6s
```

Ridiculously useful to stunlock an enemy, and consistently interrupt stuff instead of your cooldowns, or having the DPSes do it.

May have varying levels of usefulness depending on content.


**Cover (Level 45) (120s Recast)**

```
Take all damage intended for another party member.
Duration: 12s
Oath Gauge Cost: 50
Can only be executed when member is closer than 10 yalms. Does not activate with certain attacks.
```

Seems like a strong support ability, particularly if you run Paladin as off-tank. If the main tank is close to death, you use this ability to help them out.

It's an expensive ability, and you'd probably never cast it on a healer/DPS, so it's useless in 4-man dungeons because if someone's taking aggro, then you're tanking wrong in the first place.


**Divine Veil (Level 56) (90s Recast)**

```
Upon HP recovery via healing magic cast by self or a party member, a protective barrier is cast on all party members within a radius of 15 yalms.
Duration: 30s
Barrier Effect: Prevents damage up to 10% of your maximum HP
Duration: 30s
Effect ends upon additional HP recovery via healing magic.
```

Standard raid-wide barrier.


**Clemency (Level 58) (2000 Mana)**

```
Restores target's HP.
Cure Potency: 1,200
Additional Effect: Restores to self 50% of HP restored to target if target is a party member
```

Expensive single-target heal. Probably great for emergencies in an off-tank role.


**Intervention (Level 62) (10s Recast)**

```
Reduces target party member's damage taken by 10%.
Duration: 6s
Additional Effect: Increases damage reduction by another 50% of the effect of Rampart or Sentinel if either are active
Oath Gauge Cost: 50
```

Expensive single-target damage mitigation. Literally designed for use in an off-tank role (due to interaction with Rampart/Sentinel). Cannot be self-cast (I asked to verify on Discord).


**Passage of Arms (Level 70) (120s Recast)**

```
Increases block rate to 100% and creates a designated area in a cone behind you in which party members will only suffer 85% of all damage inflicted.
Duration: 18s
Effect ends upon using another action or moving (including facing a different direction).
Cancels auto-attack upon execution.
```

Seems like a crazy-strong support ability, but situational and requires group coordination.




# Dark Knight

## Unique Self-Sustain


**Shadow Wall (Level 38) (120s Recast)**

```
Reduces damage taken by 30%.
Duration: 15s
```

Standard mitigation.


**Dark Mind (Level 45) (60s Recast)**

```
Reduces magic vulnerability by 20%.
Duration: 10s
```

Magic damage mitigation.

You will probably need to learn the fights well to tell what's magic damage. Makes Dark Knight particularly strong against magic due to Dark Mind being in low cooldown.


**Living Dead (Level 50) (300s Recast)**

```
Grants the effect of Living Dead. When HP is reduced to 0 while under the effect of Living Dead, instead of becoming KO'd, your status will change to Walking Dead.
Living Dead Duration: 10s
While under the effect of Walking Dead, most attacks will not lower your HP below 1. If, before the Walking Dead timer runs out, HP is 100% restored, the effect will fade. If 100% is not restored, you will be KO'd.
Walking Dead Duration: 10s
```

Emergency invulnerability at low health.

Very difficult to use well, but amazing during emergencies.


**The Blackest Night (Level 70) (3000 Mana, 15s Recast)**

```
Creates a barrier around target that absorbs damage totaling 25% of target's maximum HP.
Duration: 7s
Grants Dark Arts when barrier is completely absorbed.
Dark Arts Effect: Consume Dark Arts instead of MP to execute Edge of Shadow or Flood of Shadow
```

Great single-target barrier. (Can also be cast on friendly, making it useful for an off-tank role.)


**Dark Missionary (Level 76) (90s Recast)**

```
Reduces magic damage taken by self and nearby party members by 10%.
Duration: 15s
```

Raid-wide magic mitigation that works on yourself as well!


## Unique Support


**The Blackest Night (Level 70) (3000 Mana, 15s Recast)**

```
Creates a barrier around target that absorbs damage totaling 25% of target's maximum HP.
Duration: 7s
Grants Dark Arts when barrier is completely absorbed.
Dark Arts Effect: Consume Dark Arts instead of MP to execute Edge of Shadow or Flood of Shadow
```

Grants single-target barrier. (Can also be cast on yourself.)


**Dark Missionary (Level 76) (90s Recast)**

```
Reduces magic damage taken by self and nearby party members by 10%.
Duration: 15s
```

Raid-wide magic mitigation.




# Gunbreaker

## Unique Self-Sustain


**Camouflage (Level 6) (90s Recast)**

```
Increases parry rate by 50% while reducing damage taken by 10%.
Duration: 20s
```

Standard mitigation.

Also interestingly makes Gunbreaker the most durable tank for low-level content due to very early unlock.


**Nebula (Level 38) (120s Recast)**

```
Reduces damage taken by 30%.
Duration: 15s
```

Standard mitigation.


**Aurora (Level 45) (60s Recast)**

```
Grants Regen to target.
Cure Potency: 200
Duration: 18s
```

Fairly ok single-target HOT, with an effective potency of 1200.


**Superbolide (Level 50) (360s Recast)**

```
Reduces HP to 1 and renders you impervious to most attacks.
Duration: 8s
```

Emergency invulnerability at low health.

Very difficult to use well, but amazing during emergencies.

Also a great way to make your healers rage at you if used poorly. Should try to coordinate in voice chat before using it.


**Heart of Light (Level 64) (90s Recast)**

```
Reduces magic damage taken by self and nearby party members by 10%.
Duration: 15s
```

Raid-wide magic mitigation that works on yourself!


**Heart of Stone (Level 68) (25s Recast)**

```
Reduces damage taken by a party member or self by 15%.
Duration: 7s
Additional Effect: When targeting a party member while under the effect of Brutal Shell, that effect is also granted to the target
Duration: 10s
```

Single-target damage mitigation that you can use on yourself.


## Unique Support


**Aurora (Level 45) (60s Recast)**

```
Grants Regen to target.
Cure Potency: 200
Duration: 18s
```

Fairly ok single-target HOT, with an effective potency of 1200.


**Heart of Light (Level 64) (90s Recast)**

```
Reduces magic damage taken by self and nearby party members by 10%.
Duration: 15s
```

Raid-wide magic mitigation.


**Heart of Stone (Level 68) (25s Recast)**

```
Reduces damage taken by a party member or self by 15%.
Duration: 7s
Additional Effect: When targeting a party member while under the effect of Brutal Shell, that effect is also granted to the target
Duration: 10s
```

Single-target damage mitigation. Low cooldown, and very effective!




# Other Differences

## Aggro Control: AOE

Warrior's standard AOE is a longer-range frontal cone instead of a shorter-range circle attack like the other tanks.

I think that this makes Warrior superior in aggro control due to this extra reach.

Do note however that Warrior is NOT limited to only frontal cone AOEs. Warrior also has short-range circle attacks such as Mythril Tempest. This gives the Warrior the flexibility to choose the best AOE for a given situation.

## Interrupt

Paladin has the only consistent CC (as far as I can tell so far) with the *Shield Bash* ability. It doesn't even have a cooldown, so you can totally deny enemies!
