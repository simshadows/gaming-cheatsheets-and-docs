# WvW Group Composition

*THIS DOCUMENT IS A DRAFT.*

```
STANDARD COMPOSITION                    ADDITIONAL SUSTAIN VARIANT

PARTY 1 (Seed Party)
1: Support Firebrand                    1: ~
2: Mesmer                               2: ~
3: Vault Spam Daredevil                 3: ~
4: Soulbeast Ranged DPS                 4: Hammer Herald/Renegade (with Dwarf Stance)
5: Spellbreaker                         5: ~

PARTY 2, 4, 6, etc. (Even Parties)
1: Support Firebrand                    1: ~
2: Healer Herald                        2: Healer Herald (with Dwarf Stance)
3: Power/Celestial Scourge              3: ~
4: Power/Celestial Scourge              4: ~
5: Spellbreaker                         5: ~

PARTY 3, 5, 7, etc. (Odd Parties)
1: Support Firebrand                    1: ~
2: Mesmer                               2: ~
3: Power/Celestial Scourge              3: Hammer Herald/Renegade (with Dwarf Stance)
4: Power/Celestial Scourge              4: ~
5: Spellbreaker                         5: ~

Also consider mixing in one Staff Elementalist for wall bombardment.
```

As the raid grows in size, add a seed party, followed by an even party, then an odd party, then an even party, etc.

Note that Party 1 uniquely contains Vault Spam Daredevil and Soulbeast Ranged DPS instead of two Power/Celestial Scourges. A group ideally only ever needs one of each of those roles to fill niches, and to inject some much-needed variety into the composition. Additionally, Vault Spam Daredevil and Soulbeast Ranged DPS are actually better for smaller-scale groups due to single-target focus and high mobility being more advantageous, conveniently smoothing the transition from small-scale to zerg-scale.

This composition is not a Pirate Ship meta composition, instead leaning more towards a melee-oriented composition with strong tactical support of Mesmers.

For additional sustain, consider adding Dwarf Stance Revenants to each party. This gives each party access to the powerful 50% damage mitigation from all sources from `Rite of the Great Dwarf`.

## Appendix: List of Common Group Roles

### Support Firebrand

Firebrands are a group's primary support role as it provides the whole package of important group support in the middle of a fight (strong heals, stability, and damage mitigation).

Primary Capabilities:

- Healing
- Stability
- Damage Mitigation
- Out-Of-Combat Swiftness

Additional Capabilities:

- Siege Quickness *(by temporarily swapping `Firebrand` traits to 2-2-x)*

### Power/Celestial Scourge

Scourges are the group's primary DPS role due to its ranged and extremely punishing wide-area AOE spike damage. However, these spikes are on a roughly 30 second cooldown (without Alacrity).

Scourges also have the option of speccing for downstate control, with minimal DPS loss. In coordination with Firebrand `Merciful Intervention`, this can allow a group to recover from the initial spike damage of an engagement.

**Please note, however, that you must only spec for downstate control if you know exactly what you are doing. Improper use can accidentally kill all your group's downs by grouping them up inside enemy AOEs.**

Primary Capabilities:

- Ranged AOE DPS
- `Sand Swell` Portals *(used out-of-combat)*

Potential Capabilities:

- Downstate Control *(must sacrifice DPS to spec into `Blood Magic` 1-x-3)*

### Hammer Herald

Hammer Herald is a ranged DPS role with highly-sustained, moderately-wide AOE damage.

Hammer Heralds can also optionally spec into the unique damage mitigation ability `Rite of the Great Dwarf` from Legendary Dwarf Stance, mitigating 50% of all incoming damage for 5 players for 4 seconds per pulse. This can be pulsed at the beginning of an engagement to mitigate the initial enemy spike before switching to a different stance for DPS.

Primary Capabilities:

- Ranged AOE DPS
- Out-Of-Combat Swiftness

Potential Capabilities:

- Damage Mitigation *(must spec into `Legendary Dwarf Stance`)*

### Mesmer

*(TODO: I don't really understand the range of capabilities of Mesmer in WvW... Are they good at single-target focus? Duelling?)*

Mesmers play a crucial support role for a group, providing portals and group stealths to give the entire group significantly more mobility and a wider range of tactical and strategic options, especially for gaining the upper hand during engagements.

Mesmers uniquely have access to a ground-targeted pull using Focus 4, optionally combined with crowd control such as `Gravity Well` from Chronomancer. This is particularly useful for pulling enemies off walls during sieges.

Mesmers can also function as scouts due to their high mobility and ability to quickly return via portal, and under many circumstances, Mesmer scouts with ready portals can be more tactically useful than the faster but portal-less Thief scouts.

Primary Capabilities:

- `Portal Entre` Portals
- Group Stealth
- High-Mobility Scout *(not as fast as Thief, but can return faster via portals.)*

Potential Capabilities:

- Wall Pull *(using Focus 4)*
- Downstate Control *(using the `Illusion of Life` utility skill)*

### Healer Herald

Healer Herald is a secondary support role focused on providing sustained, high-output healing. Herald Healers shine where raw healing output is required, such as during long-drawn, high-pressure situations where Firebrand healing drops off.

Similar to Hammer Herald, Healer Heralds can optionally spec into the unique damage mitigation ability `Rite of the Great Dwarf` from Legendary Dwarf Stance, mitigating 50% of all incoming damage for 5 players for 4 seconds per pulse. This can be pulsed at the beginning of an engagement to mitigate the initial enemy spike before switching to Centaur Stance for healing.

Primary Capabilities:

- Healer
- Siege Alacrity
- Out-Of-Combat Swiftness

Potential Capabilities:

- Damage Mitigation *(must spec into `Legendary Dwarf Stance`)*

### Spellbreaker

Spellbreaker is a cleaving melee DPS with powerful personal damage mitigation. Spellbreakers favour direct, well-coordinated charges into enemy groups where their lack of significant ranged damage and mobility is not a factor and their tankiness shines.

Spellbreaker also uniquely has access to `Winds of Disenchantment`, which provides the most devastating boonrip in the game while also negating incoming projectiles. Careful placement of this ability can turn the tide of a fight, practically killing anybody caught in the bubble.

Primary Capabilities:

- Melee Cleave DPS
- `Winds of Disenchantment`

### Vault Spam Daredevil

Vault Spam Daredevil is a cleaving melee DPS with the highest mobility in the game (both in and out of combat), though squishy and lacking the tanking capability of the Spellbreaker. Survivabilty relies on evade frame spam and mobility through vaults and shadowsteps.

It is recommended to always have at least one Daredevil in a group to act as a scout, particularly for trap-sweeping and to act as a first-responder to contest a capture point.

Primary Capabilities:

- Melee Cleave DPS
- High-Mobility Scout *(fastest for trap-sweeping and capture point contesting, but no portals)*

Potential Capabilities:

- Opening Group Stealth (using utilities and smoke fields + blast finishers)

### Soulbeast Ranged DPS

While group play heavily favours AOE/cleave builds, single-target ranged DPS can play a niche role of getting enemies into combat, and bursting vulnerable fringe targets during group fights that otherwise dodge the group's focus.

A Deadeye may also substitute for a Soulbeast, though Deadeyes lack group utility and durability.

*(TODO: Do we really need Soulbeasts when Mirage can basically do the same thing?)*

Primary Capabilities:

- Long-Range Single-Target DPS

### Roamer

This is any profession and any build that is primarily designed for solo play, and not group play (including Soulbeast Ranged DPS). <http://metabattle.com/wiki/WvW> contains a nice list of roamer builds to try (scroll down and find "Smallscale Builds").

These builds emphasize high mobility and open-field duelling capability. They're often high-damage squishy builds that rely on controlling a fight with their mobility, well-timed damage mitigation abilities, and surprise.

These builds are excellent for super-smallscale, but are generally not ideal as part of larger groups (roughly 10 or more players) due to lack of sustain, group utility, and/or lack the powerful AOE DPS of more group-oriented DPS builds.

